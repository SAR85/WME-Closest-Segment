[b]Description:[/b]
WME Closest Segment helps you determine the closest segment to the navigation stop point of a place. A line is drawn from a selected place (for point places) or its navigation point (for area places) to the nearest segment.

[b]Current version:[/b] 0.92

[b]Installation:[/b]
Use [url=https://addons.mozilla.org/en-us/firefox/addon/greasemonkey/]Greasemonkey[/url] (Firefox) or [url=https://chrome.google.com/webstore/detail/tampermonkey/dhdgffkkebhmkfjojejmpbldmpobfkfo?hl=en]Tampermonkey[/url] (Chrome) and get the script [url=https://greasyfork.org/en/scripts/8693-wme-closest-segment]here[/url].

[b]To use:[/b]
The line to the closest segment will appear on the map when a place is selected/moved. To disable the line, disable the "Closest Segment" layer in the layer selector or press [i]Ctrl+Shift+C[/i].

For best results, make sure you can see the segments on all sides of the navigation stop point on the map. If the navigation stop point is at the edge of the map, the line will be drawn to the closest segment in the visible map area but the actual closest segment could be outside of the visible map area.

Please report bugs, feature requests, or other issues. Known issues and development goals can be seen [url=https://gitlab.com/SAR85/WME-Closest-Segment]here[/url].

[b]Change log:[/b]
[List]
[*]v0.93
	[List]
	[*]Bug fix: line no longer drawn outside of map area
	[*]Bug fix: editing house numbers now works correctly with this script enabled
	[/List]
[*]v0.92
	[List]
	[*]Quick fix for last version which broke the script in Firefox
	[*]Improved finding nearest segment
	[/List]
[*]v0.91: 
	[List]
	[*]Changed the line and point styles
	[*]Added ctrl-shift-c as keyboard shortcut
	[/List]
[*]v0.9: Performance improvements
[*]v0.8: First release
[/List]